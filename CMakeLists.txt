cmake_minimum_required(VERSION 3.17)

include(./Init.cmake)

include(Policies)
set_policy(CMP0042 NEW)
set_policy(CMP0043 NEW)
set_policy(CMP0048 NEW)
include(GitVersion)

# EDITME: set your project
project(sylvac-reader
  LANGUAGES C
  HOMEPAGE_URL "https://gitlab.cern.ch/mro/common/tools/tests/sylvac-reader"
  DESCRIPTION "SYLVAC reader"
  VERSION "1.0.${GIT_COUNT}")
message(STATUS "Building ${CMAKE_PROJECT_NAME}: ${CMAKE_PROJECT_VERSION}")

include(auto_option)
include(Tools)
include(Tests)
include(Coverage)
include(Doxygen)

include(Pack)

set(CMAKE_C_STANDARD 99)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
if(NOT DEFINED BUILD_SHARED_LIBS)
    set(BUILD_SHARED_LIBS ON)
endif()

if(NOT MSVC)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pedantic")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic -Wno-long-long")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wnon-virtual-dtor -Woverloaded-virtual -Wunused-parameter -Wuninitialized")
endif(NOT MSVC)

add_subdirectory(src)

include(Lint)
