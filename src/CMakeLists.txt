
add_executable(sylvac-read main.c)

install(TARGETS sylvac-read
  RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}" COMPONENT Runtime)
