/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-08-15T17:13:29
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

int main(int argc, const char *argv[])
{
    if (argc < 2)
    {
        fputs("Usage: sylvac-read </dev/ttyUSB0>\n", stderr);
        return 1;
    }

    int fd = open(argv[1], O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (fd < 0)
    {
        perror("failed to open sylvac device");
        return 1;
    }

    struct termios p;
    if (tcgetattr(fd, &p) != 0)
    {
        perror("failed to tcgetattr");
        close(fd);
        return 1;
    }
    // cfmakeraw(&p);
    p.c_cflag = CS7 | CSTOPB | CREAD | PARENB | HUPCL | CLOCAL;
    p.c_oflag = NL0 | CR0 | TAB0 | BS0 | VT0 | FF0 | ONLCR;
    p.c_iflag = ICRNL;
    p.c_lflag = ICANON;
    p.c_cc[VMIN] = 0;
    p.c_cc[VTIME] = 0;
    cfsetspeed(&p, B4800);
    if (tcsetattr(fd, TCSADRAIN, &p) != 0)
    {
        perror("failed to tcsetattr");
        close(fd);
        return 1;
    }

    if (fcntl(STDIN_FILENO, F_SETFL,
              fcntl(STDIN_FILENO, F_GETFL, 0) | O_NONBLOCK) != 0)
    {
        perror("failed to set stdin non-blocking");
        close(fd);
        return 1;
    }
    if (isatty(STDIN_FILENO))
    {
        if (tcgetattr(STDIN_FILENO, &p) != 0)
        {
            perror("failed to tcgetattr");
            close(fd);
            return 1;
        }
        p.c_lflag &= ~(ECHO);
        if (tcsetattr(STDIN_FILENO, TCSANOW, &p) != 0)
        {
            perror("failed to tcsetattr");
            close(fd);
            return 1;
        }
    }

    int flag = TIOCM_RTS;
    ioctl(fd, TIOCMBIC, &flag);

    flag = TIOCM_DTR;
    ioctl(fd, TIOCMBIS, &flag);

    char buf[1024];
    size_t len;

    struct pollfd pfds[2] = {
        {STDIN_FILENO, POLLIN | POLLHUP | POLLERR},
        {fd, POLLIN | POLLHUP | POLLERR},
    };
    int out_fds[2] = {fd, STDOUT_FILENO};
    const size_t nfds = sizeof(pfds) / sizeof(pfds[0]);
    while (1)
    {
        if (poll(pfds, nfds, -1) < 0)
        {
            perror("failed to poll");
            close(fd);
            return 1;
        }
        for (size_t i = 0; i < nfds; ++i)
        {
            if (pfds[i].revents & POLLIN)
            {
                len = read(pfds[i].fd, buf, sizeof(buf));
                if (len > 0)
                    write(out_fds[i], buf, len);
            }
            else if (pfds[i].revents & (POLLHUP | POLLERR))
            {
                return 0;
            }
        }
    }
    return 0;
}
