# SYLVAC Reader

Basic [SYLVAC digital indicators](https://www.sylvac.ch/products/digital-indicators) reader for Linux/Unix systems

## Build

To build this project it is recommended to install [docker-builder](https://gitlab.cern.ch/mro/common/tools/x-builder) script.

To build the application:
```bash
docker-builder

rm -rf build && mkdir build
cd build && cmake3 ..

make -j4
```

## Running

To run the reader, start `sylvac-reader` and then send some carriage returns.